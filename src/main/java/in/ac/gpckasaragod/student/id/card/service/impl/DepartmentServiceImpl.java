package in.ac.gpckasaragod.student.id.card.service.impl;

import in.ac.gpckasaragod.student.id.card.model.ui.data.Department;
import in.ac.gpckasaragod.student.id.card.service.DepartmentService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author student
 */
public class DepartmentServiceImpl extends ConnectionServiceImpl implements DepartmentService{ 

    @Override
    public String saveDepartment(String name, String shortName) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO DEPARTMENT (DEPT_NAME,SHORT_NAME) VALUES "
                    + "('"+name+"','"+shortName+"')";
            System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "Save failed";
            }else{
                return "Save successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE,null,ex) ;
            return "Save failed";
        }
    }

    @Override
    public Department readDepartment(Integer id) {
      //  throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
   
         Department department = null;
        try {
            Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query ="SELECT * FROM DEPARTMENT WHERE ID = "+id;
             ResultSet resultSet = statement.executeQuery(query);
              while(resultSet.next()){
                id = resultSet.getInt("ID");
               String name = resultSet.getString("DEPT_NAME");
                String shortName = resultSet.getString("SHORT_NAME");
             department = new Department(id,name,shortName);
              }
        } 
        catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
            return department;
    }

    @Override
    public List<Department> getAllDepartments() {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      List<Department> departments = new ArrayList<>();
          try {
       Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT * FROM DEPARTMENT";
        ResultSet resultSet = statement.executeQuery(query);
          
      while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String name = resultSet.getString("DEPT_NAME");
                String shortName = resultSet.getString("SHORT_NAME");
        Department department = new Department(id,name,shortName);
            departments.add(department);
}
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
return departments;
        }

    @Override
    public String updateDepartment(Integer id, String name, String shortName) {
        try {
            //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
            Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE DEPARTMENT SET DEPT_NAME='"+name+"',SHORT_NAME='"+shortName+"' WHERE ID="+id;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            {
                return "Update failed";
            }
                else
                {
                return "Update successfully";
            }
     } catch (SQLException ex) {
            return "Update failed";
        }
      
}

    @Override
    public String DeleteDepartment(Integer Id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM DEPARTMENT WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, Id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
       
       
    }
}


