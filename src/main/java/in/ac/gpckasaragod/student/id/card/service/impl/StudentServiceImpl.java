/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.id.card.service.impl;

import com.mysql.cj.Query;
import in.ac.gpckasaragod.student.id.card.model.ui.DepartmentUiModel;
import in.ac.gpckasaragod.student.id.card.model.ui.data.Department;
import in.ac.gpckasaragod.student.id.card.model.ui.data.Student;
import in.ac.gpckasaragod.student.id.card.service.DepartmentService;
import in.ac.gpckasaragod.student.id.card.service.StudentService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class StudentServiceImpl extends ConnectionServiceImpl implements StudentService{ 

    @Override
    public String saveStudent(String name, String admissionNo, String bloodGroup, String duration, String registerNo, String address, Integer deptId) {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      try {
         Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "INSERT INTO STUDENT(STUDENT_NAME,ADMN_NO,BLOOD_GROUP,DURATION,REG_NO,ADDRESS,DEPT_ID) VALUES "+ "('"+name+"','"+admissionNo+"','"+bloodGroup+"','"+duration+"','"+registerNo+
"','"+address+"','"+deptId+"')";            
     System.err.println("Query:"+query);
            int status = statement.executeUpdate(query);
            if(status != 1){
                return "Save failed";
            }else{
                return "Save successfully";
            }
        }catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE,null,ex) ;
            return "Save failed";
        }
    }
    @Override
    public Student readStudent(Integer id) {
      //  throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
   
         Student student = null;
        try {
            Connection connection = getConnection();
               Statement statement = connection.createStatement();
               String query ="SELECT * FROM STUDENT WHERE ID = "+id;
             ResultSet resultSet = statement.executeQuery(query);
              while(resultSet.next()){
                id = resultSet.getInt("ID");
               String name = resultSet.getString("STUDENT_NAME");
                String admissionNo = resultSet.getString("ADMN_NO");
                String bloodGroup = resultSet.getString("BLOOD_GROUP");
                 String duration = resultSet.getString("DURATION");
                 String registerNo = resultSet.getString("REG_NO");
                  String address = resultSet.getString("ADDRESS");
                  Integer deptId = resultSet.getInt("DEPT_ID");
             student = new Student(deptId,id,name,admissionNo,registerNo,duration,bloodGroup,address);
              }
        } 
        catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
            return student;
    }

    @Override
    public List<DepartmentUiModel> getAllStudents() {
       // throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
      List<DepartmentUiModel> departments = new ArrayList<>();
          try {
       Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "SELECT STUDENT.ID,DEPT_NAME,SHORT_NAME,STUDENT_NAME,ADMN_NO,BLOOD_GROUP,DURATION,REG_NO,ADDRESS,DEPT_ID FROM STUDENT JOIN DEPARTMENT ON STUDENT.DEPT_ID=DEPARTMENT.ID";
        ResultSet resultSet = statement.executeQuery(query);
          
      while(resultSet.next()){
                Integer id = resultSet.getInt("ID");
                String departmentName =resultSet.getString("DEPT_NAME");
                String shortName = resultSet.getString("SHORT_NAME");
                String name = resultSet.getString("STUDENT_NAME");
                String admissionNo = resultSet.getString("ADMN_NO");
                String bloodGroup = resultSet.getString("BLOOD_GROUP");
                 String duration = resultSet.getString("DURATION");
                 String registerNo = resultSet.getString("REG_NO");
                  String address = resultSet.getString("ADDRESS");
                  Integer deptId = resultSet.getInt("DEPT_ID");
        DepartmentUiModel departmentUiModel = new DepartmentUiModel(id,departmentName,shortName,name,admissionNo,bloodGroup,duration,registerNo,address);
            departments.add(departmentUiModel);
}
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
return departments;
        }


    @Override
    public String DeleteStudent(Integer Id) {
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
        try {
            Connection connection = getConnection();
            String query = "DELETE FROM STUDENT WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, Id);
            int delete = statement.executeUpdate();
            if(delete !=1)
                return "Delete failed";
            else
                return "Deleted successfully";
            
        } catch (SQLException ex) {
           return "Delete failed";
        }
       
       
    }

    @Override
    public String updateStudent(Integer selectedId, String name,String admissionNo, String bloodGroup, String duration, String registerNo, String address, Integer deptId){
       try{
        //throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
     Connection connection = getConnection();
            Statement statement = connection.createStatement();
            String query = "UPDATE STUDENT SET STUDENT_NAME='"+name+"',ADMN_NO='"+admissionNo+"',BLOOD_GROUP='"+bloodGroup+"',DURATION='"+duration+"',REG_NO='"+registerNo+"',ADDRESS='"+address+"',DEPT_ID='"+deptId+"'WHERE ID="+selectedId;
            System.out.print(query);
            int update = statement.executeUpdate(query);
            if(update !=1)
            {
                return "Update failed";
            }
                else
                {
                return "Update successfully";
            }
     } catch (SQLException ex) {
            return "Update failed";
        }
      
    
    }

   

    

    

    }
   
