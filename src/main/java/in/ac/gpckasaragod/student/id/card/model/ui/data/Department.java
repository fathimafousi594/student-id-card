/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.id.card.model.ui.data;

/**
 *
 * @author student
 */
public class Department {
    private Integer id;
    private String departmentName;
            private String shortName;

    public Department(Integer id, String departmentName, String shortName) {
        this.id = id;
        this.departmentName = departmentName;
        this.shortName = shortName;
    }
    @Override
    public String toString(){
        
        return departmentName+"-"+shortName;
        
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }
            
    
}

