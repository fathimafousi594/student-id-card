/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.id.card.model.ui.data;

/**
 *
 * @author student
 */
public class Student {
    private Integer deptId;
     private Integer id;
    private String name;
    private String admissionNo;
    private String registerNo;
    private String duration;
    private String bloodGroup;
    private String address;

    public Student(Integer deptId, Integer id, String name, String admissionNo, String registerNo, String duration, String bloodGroup, String address) {
        this.deptId = deptId;
        this.id = id;
        this.name = name;
        this.admissionNo = admissionNo;
        this.registerNo = registerNo;
        this.duration = duration;
        this.bloodGroup = bloodGroup;
        this.address = address;
    }

    

    

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmissionNo() {
        return admissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        this.admissionNo = admissionNo;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}