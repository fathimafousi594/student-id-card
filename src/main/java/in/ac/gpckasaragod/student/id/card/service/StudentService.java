/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.student.id.card.service;

import in.ac.gpckasaragod.student.id.card.model.ui.DepartmentUiModel;
import in.ac.gpckasaragod.student.id.card.model.ui.data.Department;
import in.ac.gpckasaragod.student.id.card.model.ui.data.Student;
import java.util.List;

/**
 *
 * @author student
 */
public interface StudentService {
    public String saveStudent(String name,String admissionNo, String bloodGroup, String duration, String registerNo, String address,Integer deptId);
   public Student readStudent(Integer id);
    public List<DepartmentUiModel> getAllStudents();
    
    public  String DeleteStudent(Integer Id);

    public String updateStudent(Integer selectedId, String name,String admissionNo, String bloodGroup, String duration, String registerNo, String address, Integer deptId);



}