/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.student.id.card.model.ui;

/**
 *
 * @author student
 */
public class DepartmentUiModel {
    private  Integer id;
    private String departmentName;
            private String shortName;
            private String name;
            private String admissionNo;
            private String bloodGroup;
            private String duration;
            private String registerNo;
            private String address;

    public DepartmentUiModel(Integer id, String departmentName, String shortName, String name, String admissionNo, String bloodGroup, String duration, String registerNo, String address) {
        this.id = id;
        this.departmentName = departmentName;
        this.shortName = shortName;
        this.name = name;
        this.admissionNo = admissionNo;
        this.bloodGroup = bloodGroup;
        this.duration = duration;
        this.registerNo = registerNo;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdmissionNo() {
        return admissionNo;
    }

    public void setAdmissionNo(String admissionNo) {
        this.admissionNo = admissionNo;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRegisterNo() {
        return registerNo;
    }

    public void setRegisterNo(String registerNo) {
        this.registerNo = registerNo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    

    
}
